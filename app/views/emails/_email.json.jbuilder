json.extract! email, :id, :subject, :body, :user_id, :recipients, :created_at, :updated_at
json.url email_url(email, format: :json)
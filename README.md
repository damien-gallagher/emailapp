EmailApp
========

This is a quick demostration webapp for Alkimii by Damien Gallagher. It's a very basic email client and user management system.

User authentication is provided by Devise.

Email transactions are done through the MailGun API, using the official Mailgun Ruby gem at: https://github.com/mailgun/mailgun-ruby

Bootstrap is used to build a very basic responsive front end.

Environment Setup
-----------------

Firstly, you need to set up some environment variables to get the software running:

* EMAILAPP_DATABASE_USER - The Postgres username to create the database
* EMAILAPP_DATABASE_PASSWORD - The password to the Postgres username
* EMAILAPP_MAILGUN_SECRET_KEY - The secret key to your MailGun account
* EMAILAPP_MAILGUN_DOMAIN - The chosen domain to operate in your Mailgun account

For production servers, SECRET_KEY_BASE also needs to be configured.

Installation
------------

Use the following command to install the required Gems for the app:

```sh
bundle install
```

Then, migrate the database

```sh
bundle exec rake db:migrate
```


Testing
-------

This app provides some smokescreen tests for it's two controllers.

You can run the tests via the command:

```sh
bundle exec rake test
```

Running
-------

To run the standard Rails server use:

```sh
rails server
```

Quick Notes
-----------

* The database is empty, so you you will need to sign up a new user account when the app starts.

* When using the email sending functionality, it uses the email address of your user account for sending emails to MailGun. If that specific email address is not configured for use with MailGun, then the send will fail.

* Also note, if you are using MailGun's sandbox mode and the email address of a recipient is not validated for use with that sandbox, then that will also fail the email sending process.



class Email < ActiveRecord::Base
  # The user who composed the email
  belongs_to :user

  # The users who are recipients of the email
  #has_and_belongs_to_many :users
  has_and_belongs_to_many :recipients, class_name: 'User'

  validates :subject, presence: true
  validates :body, presence: true
  validates :recipients, presence: true
end

class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :subject
      t.text :body
      t.references :user, index: true, foreign_key: true
      t.datetime :sent_at

      t.timestamps null: false
    end

    add_index :emails, :sent_at

    create_table :emails_users, id: false do |t|
        t.belongs_to :email
        t.belongs_to :user
    end

    add_index :emails_users, :email_id
    add_index :emails_users, :user_id
  end
end

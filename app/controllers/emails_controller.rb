class EmailsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_email, only: [:show, :edit, :update, :destroy, :queue]

  # GET /emails
  # GET /emails.json
  def index
    @emails = Email.where(user: current_user).order(sent_at: 'desc')
  end

  # GET /emails/1
  # GET /emails/1.json
  def show
  end

  # GET /emails/new
  def new
    @email = Email.new
  end

  # GET /emails/1/edit
  def edit
    # A user should not be able to edit a sent email
    if !@email.sent_at.nil?
      respond_to do |format|
        format.html { redirect_to emails_url, notice: 'Email was already sent. It cannot be edited.'}
        format.json { render :show, status: :unprocessable_entity}
      end
    end
  end

  # POST /emails
  # POST /emails.json
  def create
    queue_email = false

    @email = Email.new(email_params)

    @email.user = current_user

    queue_email = true if (params[:commit] == "Send Email")

    respond_to do |format|
      if @email.save
        if !queue_email
          format.html { redirect_to @email, notice: 'Email was successfully created.' }
          format.json { render :show, status: :created, location: @email }
        else
          send_email

          @email.sent_at = DateTime.now

          if @email.save && !@mg_result.nil?
            format.html { redirect_to @email, notice: 'Email was successfully created and sent.' }
            format.json { render :show, status: :created, location: @email }
          else
            format.html { render :new }
            format.json { render json: @email.errors, status: :unprocessable_entity }
          end
        end
      else
        format.html { render :new }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emails/1
  # PATCH/PUT /emails/1.json
  def update
    queue_email = false

    queue_email = true if (params[:commit] == "Send Email")

    respond_to do |format|
      if !@email.sent_at.nil?
        format.html { redirect_to emails_url, notice: 'Email was already sent. It cannot be updated.'}
        format.json { render :show, status: :unprocessable_entity}
      elsif @email.update(email_params)
        if !queue_email
          format.html { redirect_to @email, notice: 'Email was successfully updated.' }
          format.json { render :show, status: :ok, location: @email }
        else
          send_email

          @email.sent_at = DateTime.now

          if @email.save && !@mg_result.nil?
            format.html { redirect_to @email, notice: 'Email was successfully updated and sent.' }
            format.json { render :show, status: :created, location: @email }
          else
            format.html { render :new }
            format.json { render json: @email.errors, status: :unprocessable_entity }
          end
        end
      else
        format.html { render :edit }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emails/1
  # DELETE /emails/1.json
  def destroy
    @email.destroy
    respond_to do |format|
      format.html { redirect_to emails_url, notice: 'Email was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /emails/1/send
  # GET /emails/1/send.json
  def queue
    respond_to do |format|
      if !@email.sent_at.nil?
        format.html { redirect_to emails_url, notice: 'Email was already sent. It cannot be sent again.'}
        format.json { render :show, status: :unprocessable_entity}
      else

        send_email

        @email.sent_at = DateTime.now

        if @email.save && !@mg_result.nil?
          format.html { redirect_to emails_url, notice: 'Email was successfully sent.' }
          format.json { render :show, status: :created, location: @email }
        else
          format.html { render :new }
          format.json { render json: @email.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_email
      @email = Email.find_by id: params[:id], user: current_user

      # Alert the user if we cannot retrieve the email for them
      if @email.nil?
        respond_to do |format|
          format.html { redirect_to emails_url, notice: 'Email not found.'}
          format.json { head :no_content}
        end
      end
    end

    # constructs a request to the MailGun API from the model and send it
    def send_email
      mg_client = Mailgun::Client.new

      mb_obj = Mailgun::MessageBuilder.new
      mb_obj.from @email.user.email
      mb_obj.subject @email.subject
      mb_obj.body_text @email.body

      @email.recipients.each do |r|
        mb_obj.add_recipient :to, r.email
      end

      @mg_result = mg_client.send_message(ENV['EMAILAPP_MAILGUN_DOMAIN'], mb_obj).to_h!
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def email_params
      params.require(:email).permit(:subject, :body, :user_id, {:recipient_ids => []})
    end

end
